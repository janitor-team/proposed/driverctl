driverctl (0.111-1) unstable; urgency=medium

  * New upstream 0.111 release:
    - driverctl: fix --help to return success.

 -- Luca Boccassi <bluca@debian.org>  Tue, 18 Feb 2020 16:43:27 +0000

driverctl (0.110-1) unstable; urgency=medium

  * Add gbp.conf
  * New upstream 0.110 release:
    - improve help text
    - Makefile: drop extra '/'

 -- Luca Boccassi <bluca@debian.org>  Thu, 04 Jul 2019 09:14:30 +0100

driverctl (0.108-1) unstable; urgency=medium

  * New upstream 0.108 release:
    - Argument --no-save always returns with exit code 1
    - Return error code when unbinding a device from a driver fail
    - bash-completion: autocomplete cmds starting with l.
    - bash-completion: follow the same indent rules.
    - bash-completion: add support for list- commands.
    - bash-completion: simplify and autocomplete cmds.
    - bash-completion: add support for options.
    - bash-completion: suggest pci addresses instead of driver.
  * Bump Standards-Version to 4.3.0, no changes.

 -- Luca Boccassi <bluca@debian.org>  Thu, 14 Mar 2019 11:54:27 +0000

driverctl (0.101-1) unstable; urgency=medium

  * New upstream release 0.101:
    - Make sure driverctl had loaded all the overrides before basic.target.
  * Drop all patches, merged upstream.
  * Build-depend on systemd, for systemd.pc.
  * Bump Standards-Version to 4.2.1, no changes.

 -- Luca Boccassi <bluca@debian.org>  Thu, 25 Oct 2018 17:15:43 +0100

driverctl (0.95-2) unstable; urgency=medium

  * Mark package as Multi-Arch: foreign
  * Set Rules-Requires-Root: no
  * Bump Standards-Version to 4.2.0

 -- Luca Boccassi <bluca@debian.org>  Fri, 17 Aug 2018 10:34:39 +0100

driverctl (0.95-1) unstable; urgency=medium

  * Initial packaging. (Closes: #901465)
  * Backport shellcheck fixes, Hyper-V bus fix, bash completion and
    udev installation fixes from upstream.

 -- Luca Boccassi <bluca@debian.org>  Mon, 18 Jun 2018 14:51:29 +0100
